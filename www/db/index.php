<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Control Lighting Settings</title>



    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
  <?php
  $con = new mysqli("localhost","phpscript","local","customerdb");
	
	if($con->connect_error){
		die('could not connect: '.mysql_error());
	}
	
	
	if(isset($_POST['lightOn']))
{
     $SQL = "UPDATE lights SET status='On' WHERE customerID= 1 AND lightID=1";
     $result = mysql_query($SQL);
}
	if(isset($_POST['lightOff']))
{
     $SQL = "INSERT lights SET status='Off' WHERE customerID=1 AND lightID=1'";
     $result = mysql_query($SQL);
}


if(isset($_POST['heatOn']))
{
     $SQL = "INSERT SET heating (customerID, status, maxTemp, minTemp) VALUES ('1', 'Active', ' ', ' ')";
     $result = mysql_query($SQL);
}


if(isset($_POST['heatOff']))
{
     $SQL = "INSERT INTO Heating (customerID, status, maxTemp, minTemp) VALUES ('1', 'Deactive', ' ', ' ')";
     $result = mysql_query($SQL);
}

if(isset($_POST['alarmOn']))
{
     $SQL = "INSERT INTO Alarm (customerID, status) VALUES ('1', 'Active')";
     $result = mysql_query($SQL);
}

if(isset($_POST['alarmOff']))
{
     $SQL = "INSERT INTO Alarm (customerID, status) VALUES ('1', 'Deactive')";
     $result = mysql_query($SQL);
}

if(isset($_POST['immersionOn']))
{
     $SQL = "INSERT INTO Alarm (customerID, status) VALUES ('1', 'On')";
     $result = mysql_query($SQL);
}

if(isset($_POST['immersionOff']))
{
     $SQL = "INSERT INTO Alarm (customerID, status) VALUES ('1', 'Off')";
     $result = mysql_query($SQL);
}
  
  
  
  
  
  ?>
  
  
  
  
  
  
  
  
  
  

    <div class="container-fluid">
	<div class="row">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
  <li><a data-toggle="tab" href="#menu1">Lighting Settings</a></li>
  <li><a data-toggle="tab" href="#menu2">Heating Settings</a></li>
  <li><a data-toggle="tab" href="#menu3">Alarm Settings</a></li>
  <li><a data-toggle="tab" href="#menu4">Immersion Settings</a></li>

</ul>


<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>HOME</h3>
	<form method="post">
    <p><form class="form-horizontal" role="form">
				<div class="form-group">
					 
					<label for="inputEmail3" class="col-sm-2 control-label">
						Email
					</label>
					<div class="col-sm-2">
						<input class="form-control" id="inputEmail3" type="email" />
					</div>
				</div>
				<div class="form-group">
					 
					<label for="inputPassword3" class="col-sm-2 control-label">
						Password
					</label>
					<div class="col-sm-2">
						<input class="form-control" id="inputPassword3" type="password" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">
							 
							<label>
								<input type="checkbox" /> Remember me
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						 
						<button type="submit" class="btn btn-default">
							Sign in
						</button>
					</div>
				</div>
			</form></p>




  </div>
  <div id="menu1" class="tab-pane fade">
    <h3>Lighting Settings</h3>
    <p>
		<div class="col-md-12">
			
			<div class="row">
				<div class="col-md-4">
					<p>
						Kitchen lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Dining room lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Living room lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Sitting room lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Bedroom 1 lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Bedroom 2 lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Bedroom 3 lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Bedroom 4 lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div><div class="row">
				<div class="col-md-4">
					<p>
						Outdoor lighting
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOn" value="lightOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="lightOff" value="lightOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
		
	</div></p>
  </div>
  <div id="menu2" class="tab-pane fade">
    <h3>Heating Settings</h3>
    <p>
			
			<div class="row">
				<div class="col-md-4">
					<p>
						Morning time Heating
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="heatOn" value="morningHeatOn"  class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="heatOff" value="morningHeatOff"  class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Afternoon time heating
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="heatOn" value="noonHeatOn"  class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="heatOff" value="noonHeatOff"  class="btn active btn-danger">
						Off
					</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p>
						Night time heating
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="heatOn" value="nightHeatOn" class="btn btn-success">
						On
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="heatOff" value="nightHeatOff" class="btn active btn-danger">
						Off
					</button>
				</div>
			</div></p>
  </div>

<div id="menu3" class="tab-pane fade">
    <h3>Alarm Settings</h3>
    <p><div class="row">
				<div class="col-md-4">
					<p>
						Alarm
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button" name="alarmOn" value="alarmOn" class="btn btn-success">
						Activate
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="alarmOff" value="alarmOff"  class="btn active btn-danger">
						Deactive
					</button>
				</div>


 
</div>

</p>
  </div>




<div id="menu4" class="tab-pane fade">
    <h3>Immersion Settings</h3>
    <p><div class="row">
				<div class="col-md-4">
					<p>
						Immersion
					</p>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="immersionOn" value="immersionOn"  class="btn btn-success">
						Activate
					</button>
				</div>
				<div class="col-md-2">
					 
					<button type="button"name="immersionOff" value="immersionOff"  class="btn active btn-danger">
						Deactive
					</button>
				</div>


	
				</div>	
			
		</div>

		</p>
		</form>
</div>
  
  
  
  
  
  
  




</div>




	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>
