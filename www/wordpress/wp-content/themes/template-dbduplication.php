<?php
/**
	Template Name: Home Page
*/

get_header();
get_template_part('index','banner'); ?>
<!-- Blog Section with Sidebar -->
<div class="page-builder">
	<div class="container">
		<div class="row">
			<!-- Blog Area -->
			<div class="<?php appointment_post_layout_class(); ?>" >
			<?php
			$mydb = new wpdb('phpscripts','local','customerdb','localhost');
			
			//current temp
			echo "Current temperature is :"
			$result = $mydb->get_results("SELECT temp FROM templog where timestamp = (select MAX(timestamp) from templog)");
			echo "<ul>";
				foreach ($rows as $obj) :
				   echo "<li>".$obj->temp."</li>";
				endforeach;
			echo "</ul>";
			
			//getimmersion
			echo "Current immersion status is :"
			$result = $mydb->get_results("SELECT status, mode FROM immersion where customerID = 1;");
				foreach ($rows as $obj) :
				   echo $obj->status;
				endforeach;

			//getalarmlog
			echo "Current alarm log is :"
			$result = $mydb->get_results("SELECT timestamp FROM housealarmlog where customerID = 1;");
			echo "<ul>";
				foreach ($rows as $obj) :
				   echo "<li>".$obj->timestamp."</li>";
				endforeach;
			echo "</ul>";
			
			//getalarmstatus
			echo "Current alarm status is :"
			$result = $mydb->get_results("SELECT status FROM house_alarm where customerID = 1;)");
			echo "<ul>";
				foreach ($rows as $obj) :
				   echo "<li>".$obj->Name."</li>";
				endforeach;
			echo "</ul>";
			
			//getcostatus
			echo "Current CO status is :"
			$result = $mydb->get_results("SELECT level,date_time FROM co_alert_log where customerID = 1;");
				foreach ($result as $obj) :
				   echo $obj->"level,date_time";
				endforeach;
			
			//getfirestatus
			echo "Current fire status is :"
			$result = $mydb->get_results("SELECT date_time FROM fire_alarm_log where customerID = 1;");
				foreach ($result as $obj) :
				   echo $obj->"date_time";
				endforeach;
			
			//getheatingstatus
			echo "Current heating status is :"
			$result = $mydb->get_results("SELECT status FROM heating where customerID = 1;");
				foreach ($result as $obj) :
				   echo $obj->"status";
				endforeach;
			
			//getlightstatus
			echo "Current light status is :"
			$result = $mydb->get_results("SELECT lightID, status FROM lights where customerID = 1;");
				foreach ($result as $obj) :
				   echo $obj->"lightID";
				endforeach;	
			
			?>
			<?php if( $post->post_content != "" )
			{ ?>
			<div class="blog-lg-area-left">
			<?php if( have_posts()) :  the_post();
			the_content(); 
			endif; ?>
			</div>
			<?php } comments_template( '', true ); // show comments ?>
			</div>
			<!-- /Blog Area -->			
			<!--Sidebar Area-->
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
			<!--Sidebar Area-->
		</div>
	</div>
</div>
<!-- /Blog Section with Sidebar -->
<?php get_footer(); ?>