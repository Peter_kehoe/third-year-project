For the social media interface our application uses twitter.
The library that is responisble for communication with twitter and which we used is Twython. 

Twython provides easy way to post tweets with and without images which is what we needed. 

To use this library we need APP_KEY, APP_SECRET, OAUTH_TOKEN and OAUTH_TOKEN_SECRET which we obtain by registering our application at
https://apps.twitter.com/

The two methods used are 
	update_status_with_media(status, media)
	update_status(status)

The first one is used to post tweet with image and second post tweet without image.

So to use this library we use the following code

from twython import Twython
import sys
import time

APP_KEY = 'insert_app_key_here'

APP_SECRET = 'insert_app_secret_here'

OAUTH_TOKEN = ''insert_oauth_token_here'

OAUTH_TOKEN_SECRET = 'insert_oauth_token_secret_here'

twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)

message = "FIRE ALARM"
twitter.update_status(status=message)








