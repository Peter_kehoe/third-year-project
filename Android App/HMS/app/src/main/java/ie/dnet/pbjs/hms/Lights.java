package ie.dnet.pbjs.hms;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;


public class Lights extends AppCompatActivity {
    private final String SERVER = "http://ec2-54-229-82-197.eu-west-1.compute.amazonaws.com/PHPDBScripts/";
    Switch light1;
    Switch light2;
    private DBHandler hand;
    private TextView tx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lights);
        hand = new DBHandler(this, null, null, 1);

        tx = (TextView) findViewById(R.id.lightTimers);
        light1 = (Switch) findViewById(R.id.light1);
        light2 = (Switch) findViewById(R.id.light2);

        light1.setChecked(hand.selectLightStatus(1).equals("On"));
        light1.setChecked(hand.selectLightStatus(2).equals("On"));

        light1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hand.insertLight(1, "On");
                } else {
                    hand.insertLight(1, "Off");
                }
            }
        });
        light2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hand.insertLight(2, "On");
                } else {
                    hand.insertLight(2, "Off");
                    ;
                }
            }
        });

        Runnable r = new Runnable() {
            String link = SERVER + "getTimers.php?table=light_timers";

            @Override
            public void run() {
                try {
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String line = "";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    JSONArray t = new JSONArray(sb.toString());
                    for (int i = 0; i < t.length(); i++) {
                        JSONObject temp = t.getJSONObject(i);
                        tx.append(formateDate(temp.getString("date_time")) + "---" + temp.getString("duration") + "---" + temp.get("repeat") + "\n");

                    }
                    in.close();

                } catch (Exception e) {
                    tx.append(e.getMessage());
                }
            }

            public String formateDate(String stamp) {
                String result = "";
                String[] dateTime = stamp.split(" ");
                if (dateTime.length == 2) {
                    String[] date = dateTime[0].split("-");
                    result += date[2] + "\\";
                    switch (date[1]) {
                        case "01":
                            result += "Jan\\";
                            break;
                        case "02":
                            result += "Feb\\";
                            break;
                        case "03":
                            result += "Mar\\";
                            break;
                        case "04":
                            result += "Apr\\";
                            break;
                        case "05":
                            result += "May\\";
                            break;
                        case "06":
                            result += "Jun\\";
                            break;
                        case "07":
                            result += "Jul\\";
                            break;
                        case "08":
                            result += "Aug\\";
                            break;
                        case "09":
                            result += "Sep\\";
                            break;
                        case "10":
                            result += "Oct\\";
                            break;
                        case "11":
                            result += "Nov\\";
                            break;
                        case "12":
                            result += "Dec\\";
                            break;
                    }
                    result += date[0];
                    String[] time = dateTime[1].split(":");
                    result += "\t@ " + time[0] + ":" + time[1];
                }

                return result;
            }
        };
        Thread dbThread = new Thread(r);
        dbThread.start();
    }
}

