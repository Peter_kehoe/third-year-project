package ie.dnet.pbjs.hms;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class HouseSetting extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_setting);
    }

    public void goToLight(View view){
        Intent light = new Intent(this, Lights.class);
        startActivity(light);
    }
    public void goToHeating(View view){
        Intent heat = new Intent(this, Heating.class);
        startActivity(heat);
    }
    public void goToImmersion(View view){
        Intent imm = new Intent(this, Immersion.class);
        startActivity(imm);
    }
    public void goToTimers(View view){
        Intent imm = new Intent(this, Timers.class);
        startActivity(imm);
    }
}