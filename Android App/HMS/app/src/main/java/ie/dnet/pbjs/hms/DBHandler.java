package ie.dnet.pbjs.hms;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

public class DBHandler extends SQLiteOpenHelper{

    private final String SERVER = "http://ec2-54-229-82-197.eu-west-1.compute.amazonaws.com/PHPDBScripts/";
    private static final int VERSION = 5;
    private static final String DB_NAME = "Customer.db";

    public DBHandler(Context con,String name,SQLiteDatabase.CursorFactory fac, int version){
        super(con, DB_NAME, fac, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            String qu = "CREATE TABLE currenttemp (temp INTEGER PRIMARY KEY);";
            db.execSQL(qu);
            qu = "CREATE TABLE heating (status varchar(20) PRIMARY KEY);";
            db.execSQL(qu);
            qu = "CREATE TABLE lights (id integer primary key, status varchar(20));";
            db.execSQL(qu);
            qu = "CREATE TABLE immersion (status varchar(20) PRIMARY KEY, mode varchar(20));";
            db.execSQL(qu);
            qu = "CREATE TABLE alarm (status varchar(20) PRIMARY KEY);";
            db.execSQL(qu);
            updateDB();
        }
        catch(Exception e){}
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            db.execSQL("DROP TABLE IF EXISTS currenttemp;");
            db.execSQL("DROP TABLE IF EXISTS heating;");
            db.execSQL("DROP TABLE IF EXISTS lights;");
            db.execSQL("DROP TABLE IF EXISTS immersion;");
            db.execSQL("DROP TABLE IF EXISTS alarm;");
            onCreate(db);
        }
        catch(Exception e){}
    }

    public void insertCurrenttemp(int x){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL("DELETE FROM currenttemp;");
            db.execSQL("INSERT INTO currenttemp VALUES("+x+");");
        }
        catch(Exception e){}
        db.close();
    }

    public void insertHeating(String status){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL("DELETE FROM heating;");
            db.execSQL("INSERT INTO heating VALUES(\""+status+"\");");
            sendToCentral(SERVER+"insertHeatingStatus.php?ID=1&status="+status);
        }
        catch(Exception e){
        }
    }

    public void insertLight(int id, String status){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL("DELETE FROM lights WHERE id="+id+";");
            db.execSQL("INSERT INTO lights VALUES("+id+",\""+status+"\");");
            sendToCentral(SERVER+"insertLightStatus.php?ID=1&lightID="+id+"&status="+status);
        }
        catch(Exception e){
        }
    }

    public void insertImmersion(String status, String mode){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL("DELETE FROM immersion;");
            db.execSQL("INSERT INTO immersion VALUES(\""+status+"\",\""+mode+"\");");
            sendToCentral(SERVER+"insertImmersion.php?ID=1&mode="+mode+"&status="+status);
        }
        catch(Exception e){
        }
    }

    public void insertAlarm(String status){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL("DELETE FROM alarm;");
            db.execSQL("Insert into alarm values('"+status+"')");
            sendToCentral(SERVER+"insertAlarm.php?ID=1&status="+status);
        }catch(Exception e){}
    }

    public boolean insertTimer(String type, String date,String start,String duration, boolean repeat){
        boolean inserted = false;
        try {
            String table = "";
            switch (type) {
                case "heating":
                    table = "heating_timer";
                    break;
                case "lights":
                    table = "light_timers";
                    break;
                case "immersion":
                    table = "immersion_timers";
            }
            String fDate[] = date.split("/");
            if (fDate.length == 3 && fDate[2].length() == 4) {
                sendToCentral(SERVER + "insertTimer.php?ID=1&table=" + table + "&start=" + fDate[2] + "-" + fDate[1] + "-" + fDate[0]+"%20" + start + ":00&duration=" + Integer.parseInt(duration) + "&repeat=" + repeat);
            }
            inserted = true;
        }catch(Exception e){}
        return inserted;
    }

    public void sendToCentral(String link){
        final String l = link;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(l);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(l));
                    HttpResponse response = client.execute(request);
                } catch (Exception e) {
                }
            }
        };
        Thread dbThread = new Thread(r);
        dbThread.start();
    }

    public String selectCurrentTemp(){
        SQLiteDatabase db = getWritableDatabase();
        String result="Change?";
        Cursor c = db.rawQuery("SELECT * FROM currenttemp;",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("temp"));
        db.close();
        c.close();
        return result;
    }

    public String selectHeatingStatus(){
        SQLiteDatabase db = getWritableDatabase();
        String result="Change?";
        Cursor c = db.rawQuery("SELECT status FROM heating;",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("status"));
        db.close();
        c.close();
        return result;
    }

    public String selectLightStatus(int id){
        SQLiteDatabase db = getWritableDatabase();
        String result="";
        Cursor c = db.rawQuery("SELECT status FROM lights WHERE id="+id+";",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("status"));
        db.close();
        c.close();
        return result;
    }

    public String selectImmersionMode(){
        SQLiteDatabase db = getWritableDatabase();
        String result="";
        Cursor c = db.rawQuery("SELECT mode FROM immersion;",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("mode"));
        db.close();
        c.close();
        return result;
    }

    public String selectImmersionStatus(){
        SQLiteDatabase db = getWritableDatabase();
        String result="";
        Cursor c = db.rawQuery("SELECT status FROM immersion;",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("status"));
        db.close();
        c.close();
        return result;
    }

    public String selectAlarmStatus(){
        SQLiteDatabase db = getWritableDatabase();
        String result="";
        Cursor c = db.rawQuery("SELECT status FROM alarm;",null);
        c.moveToFirst();
        if(!c.isAfterLast())
            result = c.getString(c.getColumnIndex("status"));
        db.close();
        c.close();
        return result;
    }

    public void updateDB(){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String link = SERVER+"currentTemp.php";
                try{
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    JSONArray t = new JSONArray(sb.toString());
                    JSONObject temp = t.getJSONObject(0);
                    insertCurrenttemp(temp.getInt("temp"));
                    in.close();

                }catch(Exception e){}
                link = SERVER+"getHeatingStatus.php";
                try{
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    JSONArray t = new JSONArray(sb.toString());
                    JSONObject temp = t.getJSONObject(0);
                    insertHeating(temp.getString("status"));
                    in.close();

                }catch(Exception e){}
                link = SERVER+"getLightStatus.php";
                try{
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    JSONArray t = new JSONArray(sb.toString());
                    for(int i=0;i<t.length();i++){
                        JSONObject temp = t.getJSONObject(i);
                        insertLight(temp.getInt("lightID"),temp.getString("status"));
                    }
                    in.close();

                }catch(Exception e){}
                link = SERVER+"getImmersion.php";
                try{
                    URL url = new URL(link);
                    HttpClient client = new DefaultHttpClient();
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(link));
                    HttpResponse response = client.execute(request);
                    BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();
                    JSONArray t = new JSONArray(sb.toString());
                    JSONObject temp = t.getJSONObject(0);
                    insertImmersion(temp.getString("status"), temp.getString("mode"));
                    in.close();

                }catch(Exception e){}
            }
        };
        Thread dbThread = new Thread(r);
        dbThread.start();
    }
}

