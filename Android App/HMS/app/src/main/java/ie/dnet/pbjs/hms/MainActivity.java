package ie.dnet.pbjs.hms;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBHandler han = new DBHandler(this,null,null,1);
        han.updateDB();
        setContentView(R.layout.activity_house_controls);

    }
    public void goToHouseAlarm(View view){
        Intent alarm = new Intent(this, HouseAlarm.class);
        startActivity(alarm);
    }
    public void goToControls(View view){
        Intent houseControls = new Intent(this, HouseSetting.class);
        startActivity(houseControls);
    }
    public void goToFireAlarm(View view){
        Intent fireAlarm = new Intent(this, FireAlarm.class);
        startActivity(fireAlarm);
    }
    public void goToCOAlarm(View view){
        Intent COAlarm = new Intent(this, COLog.class);
        startActivity(COAlarm);
    }
    public void goToSocialMedia(View view){
        Intent social = new Intent(this, SocialConnect.class);
        startActivity(social);
    }


}
