package ie.dnet.pbjs.hms;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Timers extends AppCompatActivity {
    TextView date;
    TextView time;
    TextView end;
    RadioGroup type;
    CheckBox repeat;
    private DBHandler hand;
    RadioButton b;
    TextView error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timers);
        hand = new DBHandler(this, null, null, 1);
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.start);
        end = (TextView) findViewById(R.id.end);
        repeat = (CheckBox) findViewById(R.id.checkBox);
        type = (RadioGroup) findViewById(R.id.group);
        error = (TextView)findViewById(R.id.Error);

    }

    public void createTimer(View view) {
        String timerType = "";
        if(type.getCheckedRadioButtonId() == R.id.heating)
            timerType = "heating";
        else if(type.getCheckedRadioButtonId() == R.id.immersion)
            timerType = "immersion";
        else if(type.getCheckedRadioButtonId() == R.id.lightTimer)
            timerType = "lights";
        if(hand.insertTimer(timerType,date.getText().toString(),time.getText().toString(),end.getText().toString(),repeat.isChecked())){
            date.setText("DD/MM/YYYY");
            time.setText("HH:MM");
            end.setText("Minutes");
            repeat.setChecked(false);
            error.setText("");
        }
        else
            error.setText("Error Creating timer");
    }
}

